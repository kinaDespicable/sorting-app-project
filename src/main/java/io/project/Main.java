package io.project;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

import static io.project.Sorting.sort;

public class Main {

    public static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        if(invalidArgs(args)){
            logger.error("Invalid number of arguments. Expected between [1, 10]. Recieved: {}", args.length);
            throw new IllegalArgumentException("Invalid arguments has been received");
        }

        logger.info("Inputs are valid. Sorting starts.");

        Integer[] array = parseIntegers(args);

        Integer[] sortedArray = sort(array);

        logger.info("Sorting completed.");

        System.out.print(Arrays.toString(sortedArray));
    }

    public static Integer[] parseIntegers(String[] input){
        Integer[] array = new Integer[input.length];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(input[i]);
        }
        return array;
    }

    public static boolean invalidArgs(String[] args){
        return (args.length == 0 || args.length > 10);
    }
}
