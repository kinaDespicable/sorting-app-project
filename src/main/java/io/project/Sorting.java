package io.project;


import java.util.Arrays;

public class Sorting {

    public static Integer[] sort(Integer[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Passed argument cannot be null");
        }
        if (array.length == 0 || array.length == 1) {
            return array;
        }

        Arrays.sort(array);

        return array;
    }

    public static boolean isSorted(Integer[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1])
                return false;
        }
        return true;
    }
}
