package io.project;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static io.project.Main.parseIntegers;
import static io.project.Sorting.isSorted;
import static io.project.Sorting.sort;
import static org.junit.Assert.*;



public class CornerCaseTest {

    private ByteArrayOutputStream sink;
    private PrintStream controlledOut;
    private ByteArrayInputStream controlledIn;
    private PrintStream defaultOut;
    private InputStream defaultIn;


    private final String[] emptyArray = new String[]{};
    private final String[] singleElementArray = new String[]{"1"};
    private final String[] tenElementArray = new String[]{"6", "2", "7", "1", "3", "8", "9", "7", "4", "10"};
    private final String[] moreThanTenElementArray = new String[]{"2", "4", "1", "6", "5", "9", "6", "8", "12", "10", "11"};




    @Test(expected = IllegalArgumentException.class)
    public void testEmptyArrayCornerCase() {
        Main.main(emptyArray);
    }

    @Test
    public void testSingleElementArrayCornerCase() {
        setControlledStreamsWithInput(singleElementArray[0]);
        try{
            Main.main(singleElementArray);
            controlledOut.flush();
            assertEquals(Arrays.toString(singleElementArray), sink.toString());
        }finally {
            setStandardStreams();
        }
    }

    @Test
    public void testTenElementArrayCornerCase() {
        setControlledStreamsWithInput(Arrays.toString(tenElementArray));
        try{
            Main.main(tenElementArray);
            controlledOut.flush();
            Integer[] result = sort(parseIntegers(tenElementArray));
            assertEquals(Arrays.toString(result), sink.toString());
        }finally {
            setStandardStreams();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenElementArrayCornerCase() {
        setControlledStreamsWithInput(Arrays.toString(tenElementArray));
        try{
            Main.main(moreThanTenElementArray);
            controlledOut.flush();
            Integer[] result = sort(parseIntegers(moreThanTenElementArray));
            assertEquals(Arrays.toString(result), sink.toString());
        }finally {
            setStandardStreams();
        }
    }

    private void setControlledStreamsWithInput(final String input) {

        sink = new ByteArrayOutputStream();
        controlledOut = new PrintStream(sink);
        controlledIn = new ByteArrayInputStream(input.getBytes());

        defaultOut = System.out;
        defaultIn = System.in;

        System.setOut(controlledOut);
        System.setIn(controlledIn);
    }

    private void setStandardStreams() {
        System.setOut(defaultOut);
        System.setIn(defaultIn);
    }

}
