package io.project;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static io.project.Sorting.isSorted;
import static io.project.Sorting.sort;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortedArraySortingTest {

    private Integer[] array;

    public SortedArraySortingTest(Integer[] array){
        this.array = array;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> sortedArrays(){
        return Arrays.asList(
                new Integer[][] { { 1, 2, 3, 4, 5, 6, 7, 8 } },
                new Integer[][] { { -5, -4, -3, -2, -1, 0 } },
                new Integer[][] { { Integer.MIN_VALUE, 0, Integer.MAX_VALUE } },
                new Integer[][] { { 0, 0, 0, 0, 0 } }
        );
    }

    @Test
    public void testSortedArray(){
        Integer[] sortedArray = sort(array);
        assertTrue(isSorted(sortedArray));
        assertArrayEquals(sortedArray, array);
    }


}
