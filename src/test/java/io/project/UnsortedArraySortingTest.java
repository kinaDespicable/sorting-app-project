package io.project;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static io.project.Sorting.isSorted;
import static io.project.Sorting.sort;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class UnsortedArraySortingTest {

    private Integer[] array;

    public UnsortedArraySortingTest(Integer[] array){
        this.array = array;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> sortedArrays(){
        return Arrays.asList(
                new Integer[][] { { 4, 3, 7, 1, 2, 0, -4, -6 } },
                new Integer[][] { { 15, -4, -3, 22, 1, 0 } },
                new Integer[][] { { Integer.MAX_VALUE, 0, Integer.MIN_VALUE } },
                new Integer[][] { { 5, 5, 5, 5, 0 } }
        );
    }

    @Test
    public void testSortedArray(){
        Integer[] sortedArray = sort(array);
        assertTrue(isSorted(sortedArray));
    }


}
